# mps-common

## 工具环境安装

详细配置过程，请参考 MPS-documents/车库管理系统2.0-微服务开发手册/车库管理系统2.0-微服务开发手册.md

```sh
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.cn,direct

go get -v github.com/micro/micro/v2
go get -v github.com/golang/protobuf/{proto,protoc-gen-go}
go get -v github.com/micro/protoc-gen-micro/v2
```

因为使用源码安装，micro/v2与go版本需要匹配，如果版本大于 go1.14，可能安装不成功，还不如直接下载预编译文件到 $GOPATH/bin

```sh
# protoc-gen-micro 没有预编译文件，步骤如下：
# step1 clone 代码（位置：$GOPATH/src/github.com/micro）
git clone https://hub.fastgit.org/micro/micro.git
# step2 切换git分支为 v2.9.3
git checkout -q v2.9.3
# step3 进入 protoc-gen-micro 目录 
cd $GOPATH/src/github.com/micro/micro/cmd/protoc-gen-micro
# step4 安装
go install

# linux
wget https://hub.fastgit.org/micro/micro/releases/download/v2.9.3/micro-v2.9.3-linux-amd64.tar.gz
wget https://hub.fastgit.org/protocolbuffers/protobuf/releases/download/v3.15.5/protoc-3.15.5-linux-x86_64.zip
wget https://hub.fastgit.org/protocolbuffers/protobuf-go/releases/download/v1.25.0/protoc-gen-go.v1.25.0.linux.amd64.tar.gz

# windows
https://hub.fastgit.org/micro/micro/releases/download/v2.9.3/micro-v2.9.3-windows-amd64.zip
https://hub.fastgit.org/protocolbuffers/protobuf/releases/download/v3.15.5/protoc-3.15.5-win64.zip
https://hub.fastgit.org/protocolbuffers/protobuf-go/releases/download/v1.25.0/protoc-gen-go.v1.25.0.windows.amd64.tar.gz
```

## protogenerator.sh使用方法

### 1. bash protogenerator.sh

不带参执行，表示对当前所有proto进行代码生成，当前proto列表为：

agv
agv-manager
area
camera
config
devices
ledscreen
log
lpr
material
opc
order
point
rack
racklot
racktype
role
route
script
segment
station
system
task
user

### 2. bash protogenerator.sh xxx

带参执行，表示对指定的proto进行代码生成.

比如根据agv.proto生成代码

```sh
  bash protogenerator.sh agv
```

## 如何go get 获取 codeup.aliyun.com 的代码库

1. 设置私有仓库为codeup.aliyun.com

```sh
go env -w GOPRIVATE=codeup.aliyun.com
```

2. 配置git，将其中GIT_USERNAME、 GIT_PASSWORD改为云效的https克隆账号

```sh
git config --global url."https://GIT_USERNAME:GIT_PASSWORD@codeup.aliyun.com".insteadOf "https://codeup.aliyun.com"
```

3. 更新各个服务中原来对 <github.com/0to1/mps-common> 的引用，改为 <codeup.aliyun.com/yfrobots/mps-common>

4. 2021-03-10 06:12:14 已经全部修改为 <codeup.aliyun.com/yfrobots/>， Dockerfile中也配置了私有仓库拉取的配置
